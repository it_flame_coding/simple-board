
//ajax 요청 시작시에 spin 시작
$(document).ajaxSend(function() {
    spinner.start();
});

//ajax 요청 끝나면 spin 종료
$(document).ajaxStop(function() {
    spinner.stop();
});


$(document).ready(function(){

    //로그인 체크
    loginCookieCheck();


    $("#loginBtn").click(function(){

        var email = $("#exampleInputEmail2").val();

        var password = $("#exampleInputPassword2").val();

        if(isEmpty(email)) {
            alert("이메일을 입력해주세요");
            $("#exampleInputEmail2").val("").focus();
            return false;
        }

        if(isEmpty(password)) {
            alert("패스워드를 입력해주세요");
            $("#exampleInputPassword2").val();
            return false;
        }


        var parameter = {
            email : email,
            password : password
        }


        commonAjax.type = "POST";
        commonAjax.url = common_setting.server + ":" + common_setting.port + "/user/get/info";
        commonAjax.data = JSON.stringify(parameter);
        commonAjax.contentType = "application/json";
        commonAjax.ajaxCall(function(_data){

            console.log("login");
            console.dir(_data);

            if(!isEmpty(_data)) {
                //로그인 이름 셋팅
                var name = _data.name + "고객님 반갑습니다!";
                console.log("name >>> :"+name);
                $("#logout .user_name").text(name);

                //쿠키를 json형식으로 저장가능하도록
                $.cookie.json = true;
                $.cookie('user', _data, { expires: 1 });

                $("#logout").show();
                $("#login").hide();
            }
        });



    });


    $(".dropdown").on("show.bs.dropdown", function(event){
        var login = $(event.relatedTarget).text(); // Get the text of the element
        console.log("login >>> :"+login);
        //alert(x);
        console.dir($(this));

        if(login.trim() == "로그인") {
            $("#login-nav").each(function(){
               this.reset();
            });
        }
    });


    $("#logout .logout-span").click(function(){
        $.removeCookie('user');
        alert("로그아웃 되셨습니다");
        $("#logout").hide();
        $("#login").show();
    });

});

function loginCookieCheck() {

    var obj = $.cookie('user');

    if(!isEmpty(obj)) {
        //console.log("loginCookieCheck >>>>> ");

        var userInfo = JSON.parse(obj);
        //console.dir(userInfo);

        if(!isEmpty(userInfo)) {

            var id = userInfo.id;

            if(!isEmpty(id)) {
                //로그인 이름 셋팅
                var name = userInfo.name + "고객님 반갑습니다!";
                //console.log("name >>> :"+name);
                $("#logout .user_name").text(name);
                $("#logout").show();
                $("#login").hide();
            }

        }
    }

}


function loginUserCheck(_id,_comment_flag) {

    var returnType = true;

    //로그인 한 유저 아이디와 실제 글쓴아이디가 같은지 비교
    var obj = $.cookie('user');
    var user_id = null;
    var userInfo = null;
    if(!isEmpty(obj)) {
        //console.log("loginCookieCheck >>>>> ");


        userInfo = JSON.parse(obj);

        if(_comment_flag) {
            return true;
        }

        if (!isEmpty(userInfo)) {
            user_id = userInfo.id;

            //수정일떄
            if(!isEmpty(_id)) {
                if(user_id != _id) {
                    //alert("자신이 쓴 글만 수정하실수 있습니다");
                    returnType = false;
                }
            //등록일떄
            } else {
                if(isEmpty(user_id)) {
                    //alert("로그인하셔야 글을 등록하실수 있습니다");
                    returnType = false;
                }
            }
        }
    } else {
        //alert("로그인하셔야 글을 쓰실수 있습니다");
        returnType = false;
    }

    return returnType;

}

function getUserID() {

    var obj = $.cookie('user');
    var user_id = null;
    if(!isEmpty(obj)) {
        //console.log("loginCookieCheck >>>>> ");

        var userInfo = JSON.parse(obj);
        //console.dir(userInfo);

        if (!isEmpty(userInfo)) {
            user_id = userInfo.id;
        }
    }

    return user_id;

}


var common_setting = {

    server : "http://localhost",
    port : "8070"

}

var spinner = {

    spinner : null,

    start : function() {

        var opts = {
            lines: 11 // The number of lines to draw
            , length: 20 // The length of each line
            , width: 10 // The line thickness
            , radius: 21 // The radius of the inner circle
            , scale: 1 // Scales overall size of the spinner
            , corners: 1 // Corner roundness (0..1)
            , color: '#000' // #rgb or #rrggbb or array of colors
            , opacity: 0.25 // Opacity of the lines
            , rotate: 0 // The rotation offset
            , direction: 1 // 1: clockwise, -1: counterclockwise
            , speed: 1 // Rounds per second
            , trail: 60 // Afterglow percentage
            , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            , zIndex: 2e9 // The z-index (defaults to 2000000000)
            , className: 'spinner' // The CSS class to assign to the spinner
            , top: '50%' // Top position relative to parent
            , left: '50%' // Left position relative to parent
            , shadow: false // Whether to render a shadow
            , hwaccel: false // Whether to use hardware acceleration
            , position: 'absolute' // Element positioning
        };

        var target = document.getElementById('SpinnerBox');
        spinner.spinner = new Spinner(opts).spin(target);
    },

    stop : function() {
        spinner.spinner.stop();
    }


}


var search = {

    search_type : null,
    tr_selector : null,
    tbody_selector : null,

    searchView : function(_page) {

        commonAjax.type = "GET";

        commonAjax.url = common_setting.server + ":" + common_setting.port;

        if(search.search_type == "user") {

            if(isEmpty(_page)) {
                commonAjax.url = commonAjax.url + "/user?page=1";
            } else {
                commonAjax.url = commonAjax.url + "/user?page=" + _page;
            }

        //게시판용
        } else if(search.search_type == "board"){

            if(isEmpty(_page)) {
                commonAjax.url = commonAjax.url + "/board?page=1";
            } else {
                commonAjax.url = commonAjax.url + "/board?page=" + _page;
            }

        }

        commonAjax.data = null;
        commonAjax.contentType = "application/json";
        commonAjax.ajaxCall(function(_data) {
            drawUi.table_create(search.tr_selector,search.tbody_selector,_data);
        });
    }
}


var drawUi = {

    tr_create : function(_tr_template_select,_tbody_selector,_data) {

        var source = $(_tr_template_select).html();

        var template = Handlebars.compile(source);

        var html = template(_data);

        $(_tbody_selector).append(html);

    },


    table_create : function(_tr_template_select,_tbody_selector,_data) {

        var data_array = [];
        var data = null;
        console.dir(_data);

        $(_tbody_selector).empty();

        //data object가 null | undefined가 아니라면
        if(!isEmpty(_data)) {


            var content = _data.content;
            //var items = data.items;
            if(isEmpty(content)) {

            } else {
                if(isObject(content)) {
                    if(isArray(content)) {
                        var items_count = content.length;
                        for(var i = 0; i < items_count; i++) {

                            var num = _data.number;
                            var size = _data.size;

                            if(num > 0) {
                                num = num * size + (i+1);
                            } else {
                                num = i + 1;
                            }

                            console.log("num >>> :"+num);

                            //게시판 타입이 user인 경우
                            if(search.search_type == "user") {

                                data = {
                                    num: num,
                                    id: content[i].id,
                                    name : content[i].name,
                                    age : content[i].age,
                                    email : content[i].email,
                                    userType: content[i].userType
                                }

                            //게시판 타입이 board인 경우
                            } else {

                                data = {
                                    num: num,
                                    name: content[i].userDTO.name,
                                    id: content[i].id,
                                    board_title : content[i].board_title,
                                    createTime : timeStampToDateConvert(content[i].createTime),
                                    updateTime : timeStampToDateConvert(content[i].updateTime)
                                }

                            }


                            data_array.push(data);
                        }

                    } else {

                    }

                }

            }

        }

        if(isEmpty(data_array)) {

        } else {
            var temp_count = data_array.length;

            for(var j = 0; j < temp_count; j++) {
                drawUi.tr_create(_tr_template_select,_tbody_selector,data_array[j]);
            }
        }


        drawUi.page_navigation_create(_data);



    },

    page_navigation_create : function(data) {
        //console.log("orderPagingDraw >>>>> ");
        console.dir(data);
        //총 레코드 갯수
        var totalCount = data.totalElements; //전체 조회 건수
        //console.log("totalCount >>> :"+totalCount);

        //현재페이지 인덱스
        var currentIndex = data.number + 1;
        //console.log("currentIndex >>> :"+currentIndex);
        // 전체 인덱스 수
        var totalIndexCount = Math.ceil(totalCount / 10);
        //console.log("totalIndexCount >>> :"+totalIndexCount);
        var preStr = "";
        var postStr = "";
        var str = "";

        var first = (parseInt((currentIndex) / 10) * 10) + 1;
        //console.log("first >>> :"+first);
        var last = (parseInt(totalIndexCount/10) == parseInt(currentIndex/10)) ? totalIndexCount%10 : 10;
        //console.log("last >>> :"+last);
        var prev = (parseInt((currentIndex-1)/10)*10) - 9 > 0 ? (parseInt((currentIndex-1)/10)*10) - 9 : 1;
        //console.log("prev >>> :"+prev);
        var next = (parseInt((currentIndex-1)/10)+1) * 10 + 1 < totalIndexCount ? (parseInt((currentIndex-1)/10)+1) * 10 + 1 : totalIndexCount;
        //console.log("next >>> :"+next);

        if(totalIndexCount > 10){ //전체 인덱스가 10이 넘을 경우, 맨앞, 앞 태그 작성
            preStr += "<li class='page-item'><a href='#this' class='page-link' onclick='_movePage(1)'>&lt;&lt;</a></li>" +
                "<li class='page-item'><a href='#this' class='pad_5' onclick='_movePage("+prev+")'>&lt;</a></li>";
        }
        else if(totalIndexCount <=10 && totalIndexCount > 1){ //전체 인덱스가 10보다 작을경우, 맨앞 태그 작성
            preStr += "<li class='page-item'><a href='#this' class='pad_5' onclick='_movePage(1)'>&lt;&lt;</a></li>";
        }

        if(totalIndexCount > 10){ //전체 인덱스가 10이 넘을 경우, 맨뒤, 뒤 태그 작성
            postStr += "<li class='page-item'><a href='#this' class='pad_5' onclick='_movePage("+next+")'>&gt;</a></li>" +
                "<li class='page-item'><a href='#this' class='pad_5' onclick='_movePage("+totalIndexCount+")'>&gt;&gt;</a></li>";
        }
        else if(totalIndexCount <=10 && totalIndexCount > 1){ //전체 인덱스가 10보다 작을경우, 맨뒤 태그 작성
            postStr += "<li class='page-item'><a href='#this' class='pad_5' onclick='_movePage("+totalIndexCount+")'>&gt;&gt;</a></li>";
        }

        if(currentIndex%10 == 0) {
            first = first - 1;
        }

        for(var i=first; i<(first+last); i++){
            if(i != currentIndex){
                str += "<li class=''><a href='#this' class='pad_5' onclick='_movePage("+i+")'>"+i+"</a></li>";
            }
            else{
                str += "<li class='active'><a href='#this' class='pad_5' onclick='_movePage("+i+")'>"+i+"</a></li>";
            }
        }

        $("#pageNavigation").empty().append(preStr + str + postStr);
    }

}


/**
 *
 * @type {{url: string, type: string, dataType: string, data: null, async: boolean, contentType: string, ajaxCall: commonAjax.ajaxCall}}
 */
var commonAjax = {
    url : "",
    type: "GET",
    dataType: "json",
    data: null,
    async: true, // true 비동기 , fasle 동기
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    ajaxCall : function (callback) {
        $.ajax({
            type: this.type,
            url: this.url,
            dataType: this.dataType,
            data: this.data,
            async: this.async,
            contentType: this.contentType,
            success: callback,
            error:function(request,status,error){
                console.log(request);
                console.log(status);
                console.log(error);
                alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
            }

        });
    }
}


//페이지 이동함수
function _movePage(value){
    console.log("_movePage >>>> :"+value);
    search.searchView(value);
}


//function loginCheck() {
//
//    var returnType = true;
//    var user = $.cookie("user");
//
//    if(typeof user != "undefined") {
//        user = JSON.parse(user);
//    }
//
//    if(isEmpty(user)) {
//        alert("로그인 하셔야 등록가능합니다");
//        returnType = false;
//    } else {
//        var user_id = null;
//
//
//        if(!isEmpty(user)) {
//            user_id = user.id;
//
//            if(isEmpty(user_id)) {
//                alert("로그인 하셔야 등록가능합니다");
//                returnType = false;
//            }
//
//        }
//    }
//
//    return returnType;
//
//}


/**
 * 인자로들어오는 timestamp는 int형
 * @param timestamp
 * @returns {string}
 */
function timeStampToDateConvert(timestamp){

    var date = new Date(timestamp);
    var year = date.getFullYear();
    var month = date.getMonth()+1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();


    //console.log("year >>>> :"+year);
    //console.log("month >>>> :"+month);
    //console.log("day >>>> :"+day);
    var retVal =   year + "-" + (month < 10 ? "0" + month : month) + "-"
        + (day < 10 ? "0" + day : day) + " "
        + (hour < 10 ? "0" + hour : hour) + ":"
        + (min < 10 ? "0" + min : min) + ":"
        + (sec < 10 ? "0" + sec : sec);

    return retVal;
}



/**
 * Null Checker
 * @param value
 * @returns {boolean}
 */
function isEmpty(value) {
    if( value == "" || value == null || value == undefined || ( value != null && typeof value == "object" && !Object.keys(value).length ) ){
        return true
    }else{
        return false
    }
}

function isObject(val) {
    if (val === null) { return false;}
    return ( (typeof val === 'function') || (typeof val === 'object') );
}

function isArray(object) {
    if (object.constructor === Array) return true;
    else return false;
}










