var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.get('/create', function(req, res) {

  var id = req.param('id');
  var title = "등록";
  if(id != "") {
    title = "수정";
  }

  res.render('create', {
    title: title,
    id: id
  });

  res.render('create', { title: 'Express' });
});

router.get('/detail', function(req, res) {

  var id = req.param('id');
  res.render('detail', {
    title: '상세',
    id: id
  });

});


module.exports = router;
