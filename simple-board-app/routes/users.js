var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('users/list',{title : "유저 목록"});
});

router.get('/create', function(req, res, next) {
  res.render('users/create',{title : "유저 등록"});
});

module.exports = router;