package ds.simple.board;


import ds.simple.board.config.Constants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.core.env.SimpleCommandLinePropertySource;

/**
 * Created by dskim on 2016. 6. 25..
 */

@SpringBootApplication
public class SimpleBoardApplication {


    public static void main(String[] args) {

        SpringApplication app = new SpringApplication(SimpleBoardApplication.class);
        SimpleCommandLinePropertySource source = new SimpleCommandLinePropertySource(args);
        addDefaultProfile(app, source);
        Environment env = app.run(args).getEnvironment();


    }


    private static void addDefaultProfile(SpringApplication app, SimpleCommandLinePropertySource source) {
        if (!source.containsProperty("spring.profiles.active") &&
                !System.getenv().containsKey("SPRING_PROFILES_ACTIVE")) {
            app.setAdditionalProfiles(Constants.SPRING_PROFILE_DEVELOPMENT);
//            app.setAdditionalProfiles(Constants.SPRING_PROFILE_TEST);
        }
    }
}
