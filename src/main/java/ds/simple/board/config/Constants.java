package ds.simple.board.config;

/**
 * Created by bdhyuk on 2016. 5. 30..
 */
public class Constants {
    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    public static final String SPRING_PROFILE_PRODUCTION = "sql/prod";
    public static final String SPRING_PROFILE_TEST = "test";
}
