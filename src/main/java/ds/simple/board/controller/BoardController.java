package ds.simple.board.controller;

import ds.simple.board.domain.Board;
import ds.simple.board.dto.BoardDTO;
import ds.simple.board.service.BoardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dskim on 2016. 6. 25..
 */
@RestController
@RequestMapping("/board")
public class BoardController {


    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BoardService boardService;


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Board save(@RequestBody Board board) {
        return boardService.save(board);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public BoardDTO detail(@PathVariable Long id) {
        return boardService.getDetail(id);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public BoardDTO update(@RequestBody Board board) {
        return boardService.update(board);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public boolean delete(@PathVariable Long id) {
        return boardService.delete(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public Page<BoardDTO> getCurrentPage(@RequestParam(value = "page", required = false) Integer pageNumber) {
        return boardService.getBoardPage(pageNumber);
    }


}
