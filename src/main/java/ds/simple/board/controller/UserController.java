package ds.simple.board.controller;


import ds.simple.board.domain.User;
import ds.simple.board.dto.UserDTO;
import ds.simple.board.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

/**
 * Created by dskim on 2016. 6. 27..
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public User save(@RequestBody User user) {
        return userService.save(user);
    }

    @RequestMapping(value = "/get/info", method = RequestMethod.POST)
    public User info(@RequestBody User user) {
        return userService.info(user);
    }


    @RequestMapping(method = RequestMethod.GET)
    public Page<UserDTO> getCurrentPage(@RequestParam(value = "page", required = false) Integer pageNumber) {
        return userService.getUserPage(pageNumber);
    }

}
