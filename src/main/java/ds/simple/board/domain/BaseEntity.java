package ds.simple.board.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity extends AbstractPersistable<Long> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_time", nullable = true, updatable = false)
    private Date createTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_time", nullable = true)
    private Date updateTime;

    @PrePersist
    public void onCreate() {
        createTime = updateTime = new Date();
    }

    @PreUpdate
    public void onUpdate() {
        updateTime = new Date();
    }
}
