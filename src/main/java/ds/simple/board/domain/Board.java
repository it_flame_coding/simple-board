package ds.simple.board.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.jws.soap.SOAPBinding;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by dskim on 2016. 6. 25..
 */
@Entity(name = "ds_board")
@Getter
@Setter
public class Board extends BaseEntity implements Serializable {


    private static final long serialVersionUID = 1L;

    /*게시판 아이디*/
    private String boardid;

    /*제목*/
    private String board_title;

    /*내용*/
    private String board_desc;

    /*유저 정보*/
    @ManyToOne
    @JsonBackReference
    private User user;

    /*댓글 */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "comment_id")
    private List<Comment> commentList;


}
