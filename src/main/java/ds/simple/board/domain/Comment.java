package ds.simple.board.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dskim on 2016. 6. 26..
 */
@Entity(name = "ds_comment")
@Getter
@Setter
public class Comment extends BaseEntity implements Serializable {

    /*게시판 아이디 */
    private String boardid;

    /*댓글 아이디*/
    @Column(name="comment_id")
    private String comment_id;

    /*댓글 내용*/
    private String comment_desc;

    @ManyToOne
    @JsonBackReference
    private User user;


}
