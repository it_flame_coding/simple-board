package ds.simple.board.domain;

import ds.simple.board.domain.type.UserType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Created by dskim on 2016. 6. 26..
 */
@Entity(name = "ds_user")
@Getter
@Setter
public class User extends BaseEntity implements Serializable {

    /*유저 이름 */
    @NotNull
    private String name;

    /*나이 */
    private String age;

    /*유저 이메일 */
    @NotNull
    private String email;

    /*유저 비밀번호 */
    @NotNull
    private String password;

    /*유저 타입*/
    @NotNull
    @Enumerated(EnumType.STRING)
    private UserType userType;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @ElementCollection(targetClass = Board.class)
    private List<Board> boardList;


    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @ElementCollection(targetClass = Comment.class)
    private List<Comment> commentList;

}
