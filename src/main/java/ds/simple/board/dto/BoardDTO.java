package ds.simple.board.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by dskim on 2016. 6. 27..
 */
@Getter
@Setter
public class BoardDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /*게시판 아이디 */
    private String boardid;

    /*제목 */
    private String board_title;

    /*내용 */
    private String board_desc;

    /*생성일 */
    private Date createTime;

    /*수정일 */
    private Date updateTime;

    /*유저정보 */
    private UserDTO userDTO;

    private List<CommentDTO> commentDTOList;

}
