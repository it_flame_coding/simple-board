package ds.simple.board.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by dskim on 2016. 6. 27..
 */
@Getter
@Setter
public class CommentDTO {

    /*게시판 아이디 */
    private String boardid;

    /*댓글 아이디*/
    private String comment_id;

    /*댓글 내용*/
    private String comment_desc;

    private UserDTO userDTO;

    /*생성일 */
    private Date createTime;

    /*수정일 */
    private Date updateTime;
}
