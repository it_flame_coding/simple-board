package ds.simple.board.dto;

import ds.simple.board.domain.type.UserType;
import lombok.Getter;
import lombok.Setter;


/**
 * Created by dskim on 2016. 6. 27..
 */
@Getter
@Setter
public class UserDTO {

    private Long id;

    /*유저 이름 */
    private String name;

    /*나이 */
    private String age;

    /*유저 이메일 */
    private String email;

    /*유저 비밀번호 */
    private String password;

    /*유저 타입 */
    private UserType userType;

}
