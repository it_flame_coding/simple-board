package ds.simple.board.repository;

import ds.simple.board.domain.Board;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by dskim on 2016. 6. 25..
 */
@Repository
public interface BoardRepository extends JpaRepository<Board, Long> {

    Board findByBoardid(String board_id);
}
