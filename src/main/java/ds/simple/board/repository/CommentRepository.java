package ds.simple.board.repository;

import ds.simple.board.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by dskim on 2016. 6. 26..
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

}
