package ds.simple.board.repository;

import ds.simple.board.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by dskim on 2016. 6. 27..
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {


    User findByEmailAndPassword(String email,String password);
}
