package ds.simple.board.service;

import ds.simple.board.domain.Board;
import ds.simple.board.domain.Comment;
import ds.simple.board.domain.User;
import ds.simple.board.dto.BoardDTO;
import ds.simple.board.dto.CommentDTO;
import ds.simple.board.dto.UserDTO;
import ds.simple.board.repository.BoardRepository;
import ds.simple.board.utils.PageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by dskim on 2016. 6. 25..
 */
@Service
@Transactional
public class BoardService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private UserService userService;

    public Board save(Board board) {


        Board data = null;

        if(!ObjectUtils.isEmpty(board.getUser())) {

            User user = userService.getUserById(board.getUser().getId());

            board.setUser(user);

            board.setBoardid(UUID.randomUUID().toString().replace("-",""));

            List<Comment> commentList = board.getCommentList();

            if(!ObjectUtils.isEmpty(commentList) && commentList.size() > 0) {

                for (Comment comment : commentList) {

                    comment.setUser(user);

                }
            }

            data = boardRepository.save(board);

        }

        return data;

    }

    public BoardDTO getDetail(long id) {

        Board board = boardRepository.findOne(id);

        BoardDTO boardDTO = new BoardDTO();
        UserDTO userDTO = new UserDTO();

        BeanUtils.copyProperties(board,boardDTO);
        BeanUtils.copyProperties(board.getUser(),userDTO);

        userDTO.setId(board.getUser().getId());

        boardDTO.setId(board.getId());
        boardDTO.setUserDTO(userDTO);

        List<CommentDTO> commentDTOList = new ArrayList<>();


        for(Comment comment : board.getCommentList()) {

            CommentDTO commentDTO = new CommentDTO();
            UserDTO commentUserDTO = new UserDTO();
            BeanUtils.copyProperties(comment,commentDTO);
            BeanUtils.copyProperties(comment.getUser(),commentUserDTO);

            commentDTO.setUserDTO(commentUserDTO);

            commentDTOList.add(commentDTO);

        }

        boardDTO.setCommentDTOList(commentDTOList);

        return boardDTO;

    }


    public BoardDTO getBoardId(String board_id) {

        Board board = boardRepository.findByBoardid(board_id);

        BoardDTO boardDTO = new BoardDTO();
        UserDTO userDTO = new UserDTO();

        BeanUtils.copyProperties(board,boardDTO);
        BeanUtils.copyProperties(board.getUser(),userDTO);

        userDTO.setId(board.getUser().getId());

        boardDTO.setId(board.getId());
        boardDTO.setUserDTO(userDTO);


        return boardDTO;
    }


    public Boolean delete(long id) {

        boolean returnType = false;

        boardRepository.delete(id);

        Board board = boardRepository.findOne(id);

        if(ObjectUtils.isEmpty(board)) {
            returnType = true;
        }

        return returnType;

    }

    public BoardDTO update(Board board) {

        BoardDTO boardDTO = new BoardDTO();

        if(!ObjectUtils.isEmpty(board.getUser())) {

            User user = userService.getUserById(board.getUser().getId());

            UserDTO userDTO = new UserDTO();
            BeanUtils.copyProperties(user,userDTO);

            Board boardData = boardRepository.findOne(board.getId());

            BeanUtils.copyProperties(board,boardData);

            boardRepository.saveAndFlush(boardData);

            BeanUtils.copyProperties(boardData,boardDTO);
            boardDTO.setUserDTO(userDTO);
            boardDTO.setId(board.getId());

        }


        return boardDTO;
    }


    public Page<BoardDTO> getBoardPage(Integer pageNumber) {

        //Page<Board> boardPage = boardRepository.findAll(pageUtil.getPageRequest(pageNumber, 10, Sort.Direction.DESC, "updateTime"));
        PageRequest pageRequest =
                new PageRequest(pageNumber - 1, 10, Sort.Direction.DESC, "updateTime");

        Page<Board> boardPage = boardRepository.findAll(pageRequest);

        List<BoardDTO> boardDTOList = new ArrayList<BoardDTO>();


        List<Board> boardList = boardPage.getContent();

        for( Board board : boardList) {

            BoardDTO boardDTO = new BoardDTO();

            UserDTO userDTO = new UserDTO();

            BeanUtils.copyProperties(board.getUser(),userDTO);
            BeanUtils.copyProperties(board,boardDTO);

            boardDTO.setId(board.getId());
            boardDTO.setUserDTO(userDTO);

            boardDTOList.add(boardDTO);
        }

        return new PageImpl<BoardDTO>(boardDTOList,pageRequest,boardPage.getTotalElements());


    }
}
