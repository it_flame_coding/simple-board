package ds.simple.board.service;

import ds.simple.board.domain.Board;
import ds.simple.board.domain.Comment;
import ds.simple.board.dto.BoardDTO;
import ds.simple.board.repository.BoardRepository;
import ds.simple.board.repository.CommentRepository;
import jdk.nashorn.internal.runtime.arrays.ArrayLikeIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dskim on 2016. 7. 3..
 */
@Service
@Transactional
public class CommentService {


    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BoardService boardService;

    @Autowired
    private UserService userService;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private CommentRepository commentRepository;

    public Comment save(Comment comment) {

        BoardDTO boardDTO = boardService.getBoardId(comment.getBoardid());

        Comment commentData = commentRepository.save(comment);

        Board board = boardRepository.findOne(boardDTO.getId());

        List<Comment> commentList = board.getCommentList();

        commentList.add(commentData);

        board.setCommentList(commentList);



        return commentData;

    }


}
