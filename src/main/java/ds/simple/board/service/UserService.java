package ds.simple.board.service;

import ds.simple.board.domain.User;
import ds.simple.board.dto.UserDTO;
import ds.simple.board.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dskim on 2016. 6. 27..
 */
@Service
@Transactional
public class UserService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    public User save(User user) {
        return userRepository.save(user);
    }

    public User info(User user) {
        return userRepository.findByEmailAndPassword(user.getEmail(),user.getPassword());
    }

    public User getUserById(Long id) {
        return userRepository.findOne(id);
    }

    public Page<UserDTO> getUserPage(Integer pageNumber) {

        //Page<Board> boardPage = boardRepository.findAll(pageUtil.getPageRequest(pageNumber, 10, Sort.Direction.DESC, "updateTime"));
        PageRequest pageRequest =
                new PageRequest(pageNumber - 1, 10, Sort.Direction.DESC, "updateTime");

        Page<User> userPage = userRepository.findAll(pageRequest);

        List<UserDTO> userDTOList = new ArrayList<UserDTO>();


        List<User> userList = userPage.getContent();

        for( User user : userList) {

            UserDTO userDTO = new UserDTO();

            BeanUtils.copyProperties(user,userDTO);

            userDTO.setId(user.getId());

            userDTOList.add(userDTO);
        }

        return new PageImpl<UserDTO>(userDTOList,pageRequest,userPage.getTotalElements());


    }

}
